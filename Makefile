.PHONY: dist test
default: help
dev:
	npm run dev

dist:
	npm run build

view:
	npm run preview

lint:
	npm run lint

new:
	npm run new


	
help:
@echo " make dev [npm run dev] dev mode" @echo "make dist [npm run build] build mode" @echo "make view [npm run preview] preview package file" @echo "make new [npm run lint] creates code through an automated process" @echo " make lint [npm run new] format check"